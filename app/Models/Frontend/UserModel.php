<?php

namespace App\Models\Frontend;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserModel extends Model
{
    protected $table='users';
    protected $fillable=['name','email','password','phone','address','avatar','id_country','level'];
}
