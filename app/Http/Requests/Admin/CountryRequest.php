<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CountryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_country'=>'required',
            'image'=>'image|mimes:jpeg,png,jpg,gif|max:1024'
        ];
    }
    public function messages()
    {
        return[
            'required'=>' Vui long nhap ten ',
            'image'=>' file phai la Hinh anh',
            'mimes'=>':attribute dinh dang file hinh anh phai la :jpeg,png,jpg,gif',
            'max'=>' file hinh anh co dung luong duoi 1mb'
        ];
    }
    public function attributes()
    {
        return[
            'name_country'=>'Ten Country',
            'image'=>'Hinh Anh'
        ];
    }
}
