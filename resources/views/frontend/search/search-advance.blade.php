@extends('frontend.layouts.app')
@section('content')
<div class="col-sm-9 padding-right">
	<h2 class="title text-center">Features Items</h2>
	<div class="" style="display:flex">
		<input type="text" class="name_search">
		<select class="price_search" >
			<option value="">Vui Long Chon</option>
			<option value="0-1000">0-1000</option>
			<option value="1000-10000">1000-10000</option>
			<option value="10000-100000">10000-100000</option>
			<option value="100000-10000000">100000-10000000</option>
		</select>
		<select class="id_category_search">
		@foreach($category as $val)
			<option value="{{$val->id}}">{{$val->name}}</option>
		@endforeach
		</select>
		<select class="id_brand_search">
		@foreach($brand as $val)
			<option value="{{$val->id}}">{{$val->name}}</option>
		@endforeach
		</select>
		<button class="search_advance">Search</button>
	</div>
<div class="features_items"><!--features_items-->
	
</div><!--features_items-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){	
	$(".search_advance").click(function(){
		var html=''
		var name =$('.name_search').val();
		var price=$('.price_search ').val();
		var category=$('.id_category_search ').val();
		var brand=$('.id_brand_search ').val();
		
		$.ajax({
			type:'post',
			url:'/ajax_search_advance',
			data:{
				'name':name,
				'brand':brand,
				'price':price,
				'category':category,
				'_token':"{{ csrf_token() }}",
			},success:function(res){
				Object.keys(res).map((key,index)=>{
					var img =JSON.parse(res[key]['image']);
					html +="<div class='col-sm-4'>"+
								"<div class='product-image-wrapper'>"+
									"<div class='single-products'>"+
										"<div class='productinfo text-center'>"+
											"<img src='/upload/product/"+img[0]+"'/>"+
											"<h2>"+res[key]['price']+"</h2>"+
											"<p>"+res[key]['name']+"</p>"+
											"<a  class='btn btn-default add-to-cart'><i class='fa fa-shopping-cart'></i>Add to cart</a>"+
										"</div>"+
										"<div class='product-overlay'>"+
											"<div class='overlay-content'>"+
												"<h2>"+res[key]['price']+"</h2>"+
												"<a href='/member/product-details/"+res[key]['id']+"'>"+res[key]['name']+"</a>"+
												"<input type='hidden' value='"+res[key]['id']+"'>"+
												"<a  class='btn btn-default add-to-cart'><i class='fa fa-shopping-cart'></i>Add to cart</a>"+
											"</div>"+
										"</div>"+
									"</div>"+
									"<div class='choose'>"+
										"<ul class='nav nav-pills nav-justified'>"+
											"<li><a ><i class='fa fa-plus-square'></i>Add to wishlist</a></li>"+
											"<li><a ><i class='fa fa-plus-square'></i>Add to compare</a></li>"+
										"</ul>"+
									"</div>"+
								"</div>"+
							"</div>";
				});
				$('.features_items').html(html);
			}
		});
	});
	// $(".search_input").keyup(function(){
	// 	var html=''
	// 	var name =$('.search_input').val();
	// 	$.ajax({
	// 		type:'post',
	// 		url:'/ajax_search',
	// 		data:{
	// 			'name':name,
	// 			'_token':"{{ csrf_token() }}",
	// 		},success:function(res){
	// 			Object.keys(res).map((key,index)=>{
	// 				var img =JSON.parse(res[key]['image']);
	// 				html +="<div class='col-sm-4'>"+
	// 							"<div class='product-image-wrapper'>"+
	// 								"<div class='single-products'>"+
	// 									"<div class='productinfo text-center'>"+
	// 										"<img src='/upload/product/"+img[0]+"'/>"+
	// 										"<h2>"+res[key]['price']+"</h2>"+
	// 										"<p>"+res[key]['name']+"</p>"+
	// 										"<a  class='btn btn-default add-to-cart'><i class='fa fa-shopping-cart'></i>Add to cart</a>"+
	// 									"</div>"+
	// 									"<div class='product-overlay'>"+
	// 										"<div class='overlay-content'>"+
	// 											"<h2>"+res[key]['price']+"</h2>"+
	// 											"<a href='/member/product-details/"+res[key]['id']+"'>"+res[key]['name']+"</a>"+
	// 											"<input type='hidden' value='"+res[key]['id']+"'>"+
	// 											"<a  class='btn btn-default add-to-cart'><i class='fa fa-shopping-cart'></i>Add to cart</a>"+
	// 										"</div>"+
	// 									"</div>"+
	// 								"</div>"+
	// 								"<div class='choose'>"+
	// 									"<ul class='nav nav-pills nav-justified'>"+
	// 										"<li><a ><i class='fa fa-plus-square'></i>Add to wishlist</a></li>"+
	// 										"<li><a ><i class='fa fa-plus-square'></i>Add to compare</a></li>"+
	// 									"</ul>"+
	// 								"</div>"+
	// 							"</div>"+
	// 						"</div>";
	// 			});
	// 			$('.features_items').html(html);
	// 		}
	// 	});
	// });	
	
});	


</script>
@endsection