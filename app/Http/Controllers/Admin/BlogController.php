<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\BlogModel;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data =BlogModel::paginate(2);
        return view ('admin/blog/blog',['data'=>$data]);   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show_create()
    {
        return view ('admin/blog/add_blog');
    }
    public function create(Request $request)
    {
       $data=$request->all();
       $file=$request->image;
       if(!empty($file))
       {
        $data['image']=$file->getClientOriginalName();
       }
       if(BlogModel::create($data))
       {
            if(!empty($file))
            {
                $file->move('upload/blog/',$file->getClientOriginalName());
            }
            return redirect()->back()->with('success','Them Blog thanh cong');
       }else{
            return redirect()->back()->withErrors('Them Blog That bai');
       }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_edit($id)
    {
        $data=BlogModel::FindOrFail($id);
        return view ('Admin/Blog/edit_blog',['data'=>$data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $data= $request->all();
        $file=$request->image;
        $table=BlogModel::FindOrFail($id);
        if(!empty($file))
        {
            $data['image']=$file->getClientOriginalName();
        }
        if($table->update($data))
        {
            if(!empty($file))
            {
                $file->move('/upload/blog',$file->getClientOriginalName());
            }
            return redirect()->back()->with('success','Edit Blog thanh cong');
        }else{
            return redirect()->back()->withErrors('Edit Blog that bai');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=BlogModel::find($id);
        if($data->delete())
        {
            return redirect()->back()->with('success','delete blog thanh cong');
        }else{
            return redirect()->back()->withErrors('delete blog that bai');
        }
    }
}
