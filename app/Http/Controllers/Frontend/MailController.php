<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Frontend\HistoryModel;
use Mail;
use App\Mail\MailNotify;
use App\Models\Frontend\UserModel;
use App\Models\Admin\CountryModel;
use Auth;
use App\Http\Requests\Frontend\RegisterRequest;
class MailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $data=[
            'subject'=>'Cambo Tutorial Mail',
            'body'=>'Hello This is my email Delivery!'

        ];
        try {
            Mail::to('twitchhao03@gmail.com')->send(new MailNotify($data));
            return response()->json(['great check your mail box']);
        } catch ( Exception $th) {
            return response()->json(['sorry']);
        }
    }
   
    public function register(RegisterRequest $request) {   
        if(!Auth::check()) {
            $data= $request->all();
            $file=$request->file();
            $data['level']=0;
            $id =UserModel::orderBy('id','desc')->take(1)->get();
            $check=0;
            foreach($id as $value){
                $data['id_user']=$value->id+1;
            }
            if(!empty($file)) {
                $data['avatar']=$file->getClientOriginalName();
            }
            if(!empty($data['password'])) {
                $data['password']=bcrypt($data['password']);
            }            
            $mail=[
                'subject'=>'Cambo Tutorial Mail',
                'body'=>'Hello This is my email Delivery!'
                ];
            try {

                Mail::to('twitchhao03@gmail.com')->send(new MailNotify($mail));
                if(UserModel::create($data)) {
                    if(!empty($file)) {
                    $file->move('upload/user/avatar/',$file->getClientOriginalName());
                    }
                }
                    HistoryModel::create($data);                
                return redirect()->back()->with('success','Dang Ky va Checkout thanh cong');
            }catch( Exception $th) {
                return redirect()->back()->withErrors('Dang Ky va Checkout that bai');
            }
        }  
        else {
           
            $data['email']=Auth::user()->email;
            $data['phone']=Auth::user()->phone;
            $data['name']=Auth::user()->name;
            $data['id_user']=Auth::user()->id;
            $data['price']=$check->price;
            $mail=[
                'subject'=>'Cambo Tutorial Mail',
                'body'=>'Hello This is my email Delivery!'
                ];
            try {

                Mail::to('twitchhao03@gmail.com')->send(new MailNotify($mail));
                HistoryModel::create($data);
                return redirect()->back()->with('success','Checkout thanh cong');
            }catch( Exception $th) {
                return redirect()->back()->withErrors('Checkout that bai');
            } 
        }

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
     public function show_checkout(Request $request){
        $country=CountryModel::all();
        $table=$request->session()->get('cart');
        return view ('/frontend/cart/checkout',['cart'=>$table,'country'=>$country]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
