<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\CountryRequest;
use App\Models\Admin\CountryModel;
use Illuminate\Support\Facades\Auth;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $data=CountryModel::paginate(2);
        return view ('admin/country/country',['data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show_add_country()
    {
        return view ('admin/country/add_country');
    }
    public function add_country(CountryRequest $request)
    {
        $data=$request->all();
        $file=$request->image;
        if(!empty($file))
        {
            $data['image']=$file->getClientOriginalName();
            
        }
        if(CountryModel::create($data))
        {
            if(!empty($file))
            {
                $file->move('upload/country/', $file->getClientOriginalName());
            }
            return redirect()->back()->with('success','Them Country Thanh Cong');
        }else{
            return redirect()->back()->withErrors('Them Country that bai');
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_edit($id)
    {
        $table=CountryModel::FindOrFail($id);
        return view('admin/country/edit_country',['table'=>$table]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(CountryRequest $request, $id)
    {
        $table=CountryModel::FindOrFail($id);
        $data=$request->all();
        $file =$request->image;
        if(!empty($file))
        {
            $data['image']=$file->getClientOriginalName();
        }
        if($table->update($data))
        {
            if(!empty($file))
            {
                $file->move('upload/country/', $file->getClientOriginalName());
            }
            return redirect()->back()->with('success','Edit Country thanh cong');
        }else{
            return redirect()->back()->withErrors('Edit Country that bai');
        }
        
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $table=CountryModel::find($id);
        if($table->delete())
        {
            return redirect()->back()->with('success','Xoa Thanh Cong');
        }
        else{
            return redirect()->back()->withErrors('Xoa that bai');
        }
    }
}
