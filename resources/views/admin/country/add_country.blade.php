@extends('admin.layouts.app')
@section('content')
	<div class="page-breadcrumb">
	    <div class="row">
	        <div class="col-5 align-self-center">
	            <h4 class="page-title">Add Country</h4>
	        </div>
	        <div class="col-7 align-self-center">
	            <div class="d-flex align-items-center justify-content-end">
	                <nav aria-label="breadcrumb">
	                    <ol class="breadcrumb">
	                        <li class="breadcrumb-item">
	                            <a href="#">Home</a>
	                        </li>
	                        <li class="breadcrumb-item active" aria-current="page">Add Country</li>
	                    </ol>
	                </nav>
	            </div>
	        </div>
	    </div>
	</div>
	<div class="col-lg-8 col-xlg-9 col-md-7">
	    <div class="card">
	        <div class="card-body">
	            <form class="form-horizontal form-material" method="POST" enctype="multipart/form-data">
	                @csrf
	                <div class="form-group">
	                    <label class="col-md-12">Name Country</label>
	                    <div class="col-md-12">
	                        <input type="text" placeholder="" class="form-control form-control-line " name="name_country" value="">
	                    </div>
	                </div>
                    <div class="form-group">
                        <label class="col-md-12">Description</label>
                        <div class="col-md-12">
                            <textarea rows="5" class="form-control form-control-line" name="description"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-12">Select Image</label>
                        <div class="col-sm-12">
                        <input type="file" name="image"/>    
                        </div>
                    </div>                      	                                                    
	                <div class="form-group">
	                    <div class="col-sm-12">
	                        <button type="submit" class="btn btn-success">Add Country</button>
	                    </div>
	                </div>
	            @if($errors->any())
	                    <div class="alert alert-danger alert-dismissible">
	                                           
	                        <ul>
	                            @foreach($errors->all() as $error)
	                                <li>{{$error}}</li>
	                            @endforeach
	                        </ul>
	                    </div>
	            @endif  
	            @if(session('success'))
	            <p class="alert alert-success">{{session('success')}}</p>
	            @endif                                   
	            </form>
	        </div>
	    </div>
	</div>   
@endsection