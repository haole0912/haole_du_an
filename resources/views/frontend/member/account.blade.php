@extends('frontend.layouts.app')
@section('content')
<div class="col-sm-9">
<div class="blog-post-area">
			<h2 class="title text-center">Update user</h2>
		<div class="signup-form">
			<h2>Update User</h2>
			<form method="POST">
				@csrf
				<input type="text" placeholder="Name" name="name" value="{{$table->name}}" />
				<input type="email" placeholder="Email Address" name="email" value="{{$table->email}}" />
				<input type="password" placeholder="Password" name="password" />
				<input type="text"  placeholder="Phone No" name="phone" value="{{$table->phone}}">
				<input type="text"  placeholder="Address" name="address" value="{{$table->address}}">
				<select name="id_country">
					@foreach($country as $value)
					@if($table->id_country==$value->id)
					<option selected="true" value="{{$value->id}}">{{$value->name_country}}</option>
					@else
					<option value="{{$value->id}}">{{$value->name_country}}</option>
					@endif
					@endforeach
				</select>
				<label>Avatar</label>
				<input type="file" name="avatar">
				<button type="submit" class="btn btn-default">Update</button>
				@if($errors->any())								
						<div class="alert alert-danger alert-dismissible">
							<ul>
								@foreach($errors->all() as $error )
								<li>{{$error}}</li>
								@endforeach
							</ul>
						</div>
				@endif
				@if(session('success'))
					<h1 class="alert alert-success">{{session('success')}}</h1>
				@endif
			</form>
		</div>
	</div>	
</div>	
@endsection