<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Frontend\ProductModel;
class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ajax_add_to_cart(Request $request)
    {
        $table=ProductModel::FindOrFail($request->id);
        $table['qty']=1;
        $table['tong']=1;
        $table['tong']=$table['qty']*$table['price'];
        $check=2;
        if($request->session()->has('cart')){
        $get=$request->session()->get('cart');
        foreach ($get as $key=>$value){
            if($request->id==$value->id)
            {  
              $get[$key]['qty']+=1;
              $get[$key]['tong']=$get[$key]['qty']*$get[$key]['price'];
              $check=1;  
            }
        }
        if($check==2){
            $get[]=$table;
        }
        $request->session()->put('cart',$get); 
        }else{

        $array[]=$table;
        $request->session()->put('cart',$array);   
        }
        return $request->session()->get('cart');

        //$request->session()->forget('cart');
    }    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show_cart(Request $request)
    {
        $table=$request->session()->get('cart');
        
        return view ('/frontend/cart/cart',['cart'=>$table]);
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function ajax_cart(Request $request){
        if($request->button=="up"){
            $get=$request->session()->get('cart');
            foreach ($get as $key=>$value){
                if($request->id_product==$value->id)
                {  
                  $get[$key]['qty']+=1;
                  $get[$key]['tong']=$get[$key]['qty']*$get[$key]['price'];
                  return $get[$key]['qty'];
                  
                }
            }
            $request->session()->put('cart',$get);
        }
        if($request->button=="down"){
            $get=$request->session()->get('cart');
            foreach ($get as $key=>$value){
                if($request->id_product==$value->id)
                {  
                  if($get[$key]['qty']>0){
                    $get[$key]['qty']-=1;
                    $get[$key]['tong']=$get[$key]['qty']*$get[$key]['price'];
                    return $get[$key]['qty'];

                  }else{
                    unset($get[$key]);
                  }   
                }
            }
            $request->session()->put('cart',$get);
        }
        if($request->button=="delete"){
            $get=$request->session()->get('cart');
            foreach($get as $key=>$value){
                if($request->id_product==$value->id){
                    unset($get[$key]);
                }
            }
            $request->session()->put('cart',$get);
            return 'xoa qty thanh cong';
        }        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
