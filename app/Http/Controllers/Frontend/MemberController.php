<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Frontend\UserModel;
use App\Models\Admin\CountryModel;
use Illuminate\Support\Facades\Auth;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show_reg()
    {
        $country=CountryModel::all();
        return view ('/frontend/member/register',['country'=>$country]);
    }
    public function show_log()
    {
        return view ('/frontend/member/login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $data= $request->all();
        $file=$request->file();
        $data['level']=0;
        if(!empty($file))
        {
            $data['avatar']=$file->getClientOriginalName();
        }
        if(!empty($data['password']))
        {
            $data['password']=bcrypt($data['password']);
        }
        if(UserModel::create($data))
        {
            if(!empty($file))
            {
                $file->move('upload/user/avatar/',$file->getClientOriginalName());
            }
            return redirect()->back()->with('success','Dang Ky thanh cong');
        }else{
            return redirect()->back()->withErrors('Dang ky that bai');
        }
    }
    public function login(Request $request)
    {   
        
        $login =[
            'email'=>$request->email,
            'password'=>$request->password,
            'level'=>0
        ];
        $remeber=false;
        if($request->remeber_me)
        {
            $remeber=true;
        }
        if(Auth::attempt($login,$remeber))
        {
           return redirect()->back()->with('success','login thanh cong'); 
        }
        else {
            return redirect()->back()->withErrors('Login That bai');
        }
    }
    public function logout () {
            Auth::logout();
            return redirect('member/login')->with('success','Logout Thanh cong');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show_account() {
        if(Auth::check()) {
            $id=Auth::user()->id;
            $table=UserModel::FindOrFail($id);
            $country=CountryModel::all();
            return view ('frontend/member/account',['table'=>$table,'country'=>$country]);
        }

    }
    public function update(Request $request,$id)
    {
        $data = $request->all();
        $file=$request->avatar();
        if(!empty($file)) {
            $data['avatar']=$file->getClientOriginalName();
        }
        if(!empty($data['password']))
        {
            $data['password']=bcrypt($data['password']);
        }
        if(UserModel::update($data)) {
            if(!empty($file))
            {
                $file->move('upload/user/avatar/',$file->getClientOriginalName());
            }
            return redirect()->back()->with('success','Update thanh cong');
        }else{
            return redirect()->back()->withErrors('Update that bai');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
