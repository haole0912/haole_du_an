@extends('frontend.layouts.app')
@section('content')
<section id="form">
	<div class="container">
		<div class="row">
			<div class="col-sm-4 col-sm-offset-1">
					<div class="login-form"><!--login form-->
						<h2>Login to your account</h2>
						<form method="POST">
							@csrf
							<input type="email" placeholder="Email Address" name="email" />
							<input type="password" placeholder="Password" name="password">
							<span>
								<input type="checkbox" name="remember_me" class="checkbox"> 
								Keep me signed in
							</span>
							<button type="submit" class="btn btn-default">Login</button>
							@if($errors->any())								
									<div class="alert alert-danger alert-dismissible">
										<ul>
											@foreach($errors->all() as $error )
											<li>{{$error}}</li>
											@endforeach
										</ul>
									</div>
							@endif
							@if(session('success'))
								<h1 class="alert alert-success">{{session('success')}}</h1>
							@endif
						</form>
					</div><!--/login form-->
				</div>
		</div>
	</div>		
</section>
@endsection