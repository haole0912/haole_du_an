@extends('frontend.layouts.app')
@section('content')
<?php 
	$html="";
	if(!empty($cart)){
		foreach($cart as $value){
		$img=json_decode($value->image);
		$html.='	<tr class="tr_cart" id="'.$value->id.'">
						<td class="cart_product">
							<a href=""><img src="/upload/product/hinh85_'.$img[0].'" alt=""></a>
						</td>
						<td class="cart_description">
							<h4><a href="">'.$value->name.'</a></h4>
							<p>Web ID: '.$value->id.'</p>
						</td>
						<td class="cart_price">
							<p class="price">$'.$value->price.'</p>
						</td>
						<td class="cart_quantity">
							<div class="cart_quantity_button" >
								<a class="cart_quantity_up" href="#/"> + </a>
								<input class="cart_quantity_input" type="text" name="quantity" value="'.$value->qty.'" autocomplete="off" size="2">
								<a class="cart_quantity_down" href="#/"> - </a>
							</div>
						</td>
						<td class="cart_total">
							<p class="cart_total_price">$'.$value->tong.'</p>
						</td>
						<td class="cart_delete" id="'.$value->id.'">
							<a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
						</td>
					</tr>';
		}
	}
	

?>
<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="">Home</a></li>
				  <li class="active">Check out</li>
				</ol>
			</div><!--/breadcrums-->			
			<div class="shopper-informations">
			<div class="row">
				
				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
						
						<form method="POST">
							@csrf
							@if(!Auth::check())
								<h2>New User Signup!</h2>
								<input type="text" placeholder="Name" name="name" />
								<input type="email" placeholder="Email Address" name="email" />
								<input type="password" placeholder="Password" name="password" />
								<input type="text"  placeholder="Phone No" name="phone">
								<input type="text"  placeholder="Address" name="address">
								<select name="id_country">
									@foreach($country as $value)
									<option value="{{$value->id}}">{{$value->name_country}}</option>
									@endforeach
								</select>
								<label>Avatar</label>
								<input type="file" name="avatar">
							@else
								<input type="hidden" name="name" value="a">
								<input type="hidden" name="email" value="a@gmail.com">
								<input type="hidden" name="phone" value="1">
								<input type="hidden" name="address" value="a">
							@endif
								<input type="hidden" name="price" class="price" value="">
								<button type="submit" class="btn btn-default" name="action" value="signup">Continue</button>
								@if($errors->any())								
										<div class="alert alert-danger alert-dismissible">
											<ul>
												@foreach($errors->all() as $error )
												<li>{{$error}}</li>
												@endforeach
											</ul>
										</div>
								@endif
								@if(session('success'))
									<h1 class="alert alert-success">{{session('success')}}</h1>
								@endif
						</form>
					</div><!--/sign up form-->
				</div>			
		</div>
			</div>
			<div class="review-payment">
				<h2>Review & Payment</h2>
			</div>

			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						{!!$html!!}
						<tr>
							<td colspan="4">&nbsp;</td>
							<td colspan="2">
								<table class="table table-condensed total-result">
									<tr>
										<td>Cart Sub Total</td>
										<td>$59</td>
									</tr>
									<tr>
										<td>Exo Tax</td>
										<td>$2</td>
									</tr>
									<tr class="shipping-cost">
										<td>Shipping Cost</td>
										<td>Free</td>										
									</tr>
									<tr>
										<td>Total</td>
										<td><span class="total">$61</span></td>
									</tr>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="payment-options">
					<span>
						<label><input type="checkbox"> Direct Bank Transfer</label>
					</span>
					<span>
						<label><input type="checkbox"> Check Payment</label>
					</span>
					<span>
						<label><input type="checkbox"> Paypal</label>
					</span>
				</div>
		</div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(e){
		$('.cart_quantity_up').click(function(){
			var total_price=0;
			var id_product =$(this).closest('.tr_cart').attr('id');
			var sl=$(this).next().val();
			var price=$(this).closest('.tr_cart').find('.price').text().replace('$','');
			sl =parseFloat(sl)+1;
			$(this).closest('.tr_cart').find('.cart_quantity_input').val(sl);
			var tong=sl*price;	
			$(this).closest('.tr_cart').find('.cart_total_price').text(tong);
			$(".cart_total_price").each(function(){

				 total_price+=parseFloat($(this).text().replace('$',''));
				 
				});
				total_price+=2;				
				$('span.total').text('$'+total_price);	
				$('.price').val(total_price);
			// $.ajax({
			// 	type:'post',
			// 	url :'/up_ajax_cart',
			// 	data:{
			// 		"id_product":id_product,
			// 		"button":"up",
			// 		"_token": "{{ csrf_token() }}",
			// 	},success:function(res){
			// 		console.log(res);
			// 	},error:function(res){
			// 		console.log(res);
			// 	},
			// });
		});
		$('.cart_quantity_down').click(function(){
			var total_price=0;
			var id_product =$(this).closest('.tr_cart').attr('id');	
			var sl=$(this).prev().val();
			if(sl>1){
				sl =parseFloat(sl)-1;
				var price=$(this).closest('.tr_cart').find('.price').text().replace('$','');
				$(this).closest('.tr_cart').find('.cart_quantity_input').val(sl);
				var tong=sl*price;	
				$(this).closest('.tr_cart').find('.cart_total_price').text(tong);

				$(".cart_total_price").each(function(){

				 total_price+=parseFloat($(this).text().replace('$',''));
				 
				});
				total_price+=2;				
				$('span.total').text('$'+total_price);
				$('.price').val(total_price);
			}
			// $.ajax({
			// 	type:'post',
			// 	url :'/down_ajax_cart',
			// 	data:{
			// 		"id_product":id_product,				
			// 		"button":"down",
			// 		"_token":"{{ csrf_token() }}",
			// 	},success:function(res){
			// 		console.log(res);
			// 	},error:function(res){
			// 		console.log(res);
			// 	}
		 // 	});
		});
		$(".cart_quantity_delete").click(function(e){
				var id_product =$(this).closest('.tr_cart').attr('id');			
				// $.ajax({
				// 	type:'post',
				// 	url :'/delete_ajax_cart',
				// 	data:{
				// 		"id_product" :id_product,
				// 		"button" :"delete",
			 // 			_token: "{{ csrf_token() }}",						
				// 		},success : function(res){
				// 			console.log(res);
				// 		},error:function(res){
				// 			console.log(res);
				// 		}
				// });	
			});	
		
			var total=0;
			$(".cart_total_price").each(function(){

				 total+=parseFloat($(this).text().replace('$',''));				
				 
			});
			total+=2;				
			$('span.total').text('$'+total);
			$('.price').val(total);
					// $(this).closest('.tr_cart').find('.price').val(res);
					// var price=$(this).closest('.tr_cart').find('.price').text().replace('$','');
					// var sl=$(this).closest('.tr_cart').find('.cart_quantity_input').val();
					// var tong=sl*price;	
					// $(this).closest('.tr_cart').find('.cart_total_price').text(tong);		
	});
</script>	
@endsection