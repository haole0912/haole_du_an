@extends('frontend.layouts.app')
@section('content')
<?php 
	$img =json_decode($product->image);
	$count=count($img);
	$html="";
	if($count==1) {
		$html='<div class="item active">
					 <a ><img  src="/upload/product/hinh85_'.$img[0].'" alt=""></a>
					</div>
					<div class="item">
					 <a ><img  src="/upload/product/hinh85_'.$img[0].'" alt=""></a>
					</div>
					<div class="item">
					  <a ><img  src="/upload/product/hinh85_'.$img[0].'" alt=""></a>
					</div>';
	}
	if($count==2) {
		$html='<div class="item active">
					  <a ><img  src="/upload/product/hinh85_'.$img[0].'" alt=""></a>
					  <a ><img  src="/upload/product/hinh85_'.$img[1].'" alt=""></a>
					 
					</div>
					<div class="item">
					  <a ><img  src="/upload/product/hinh85_'.$img[0].'" alt=""></a>
					  <a ><img  src="/upload/product/hinh85_'.$img[1].'" alt=""></a>
					 
					</div>
					<div class="item">
					 <a ><img  src="/upload/product/hinh85_'.$img[0].'" alt=""></a>
					 <a ><img  src="/upload/product/hinh85_'.$img[1].'" alt=""></a>
					  
					</div>';
	}
	if($count==3) {
		$html='<div class="item active">
					  <a ><img  src="/upload/product/hinh85_'.$img[0].'" alt=""></a>
					  <a ><img  src="/upload/product/hinh85_'.$img[1].'" alt=""></a>
					  <a ><img  src="/upload/product/hinh85_'.$img[2].'" alt=""></a>
					</div>
					<div class="item">
					  <a ><img  src="/upload/product/hinh85_'.$img[0].'" alt=""></a>
					  <a ><img  src="/upload/product/hinh85_'.$img[1].'" alt=""></a>
					  <a ><img  src="/upload/product/hinh85_'.$img[2].'" alt=""></a>
					</div>
					<div class="item">
					  <a ><img  src="/upload/product/hinh85_'.$img[0].'" alt=""></a>
					  <a ><img  src="/upload/product/hinh85_'.$img[1].'" alt=""></a>
					  <a ><img  src="/upload/product/hinh85_'.$img[2].'" alt=""></a>
					</div>';
	}
?>
<div class="product-details"><!--product-details-->
	<div class="col-sm-5">
		<div class="view-product">
			<img src="/upload/product/{{$img[0]}}"  alt="" class="to" />
			<a href="/upload/product/{{$img[0]}}"  rel="prettyPhoto" ><h3>ZOOM</h3></a>		
		</div>
		<div id="similar-product" class="carousel slide" data-ride="carousel">
			
			  <!-- Wrapper for slides -->
			    <div class="carousel-inner">
					{!! $html !!}
				</div>
			  <!-- Controls -->
			  <a class="left item-control" href="#similar-product" data-slide="prev">
				<i class="fa fa-angle-left"></i>
			  </a>
			  <a class="right item-control" href="#similar-product" data-slide="next">
				<i class="fa fa-angle-right"></i>
			  </a>
		</div>

	</div>
	<div class="col-sm-7">
		<div class="product-information"><!--/product-information-->
			<img src="images/product-details/new.jpg" class="newarrival" alt="" />
			<h2>{{$product->name}}</h2>
			<p>Web ID: {{$product->id}}</p>
			<img src="images/product-details/rating.png" alt="" />
			<span>
				<span>US ${{$product->price}}</span>
				<label>Quantity:</label>
				<input type="text" value="3" />
				<button type="button" class="btn btn-fefault cart">
					<i class="fa fa-shopping-cart"></i>
					Add to cart
				</button>
			</span>
			<p><b>Availability:</b> In Stock</p>
			<p><b>Condition:</b> New</p>
			<p><b>Brand:</b> E-SHOPPER</p>
			<a href=""><img src="/frontend/images/product-details/share.png" class="share img-responsive"  alt="" /></a>
		</div><!--/product-information-->
	</div>
</div><!--/product-details-->
<script src="{{asset('frontend/js/jquery-1.9.1.min.js')}}"></script> 		
<script type="text/javascript" charset="utf-8">
    	$(document).ready(function(){
		    $("a[rel^='prettyPhoto']").prettyPhoto();
		    $('img').click(function(){
		    	var click=$(this).attr('src');
		    	const src =click.replace('hinh85_','');
		    	$('.to').attr('src',src);
		    	$('.to').next().attr('href',src);
		    });
		});
</script>
@endsection