<?php

namespace App\Models\Frontend;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductModel extends Model
{
    protected $table='product';
    protected $fillable=['image','name','price','id_category','id_brand','sale','details','company'];
}
