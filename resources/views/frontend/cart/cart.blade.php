@extends('frontend.layouts.app')
@section('content')
<?php 
	$html="";
	if(!empty($cart)){
		foreach($cart as $value){
		$img=json_decode($value->image);
		$html.='	<tr class="tr_cart" id="'.$value->id.'">
						<td class="cart_product">
							<a href=""><img src="/upload/product/hinh85_'.$img[0].'" alt=""></a>
						</td>
						<td class="cart_description">
							<h4><a href="">'.$value->name.'</a></h4>
							<p>Web ID: '.$value->id.'</p>
						</td>
						<td class="cart_price">
							<p class="price">$'.$value->price.'</p>
						</td>
						<td class="cart_quantity">
							<div class="cart_quantity_button" >
								<a class="cart_quantity_up" href="#"> + </a>
								<input class="cart_quantity_input" type="text" name="quantity" value="'.$value->qty.'" autocomplete="off" size="2">
								<a class="cart_quantity_down" href="#"> - </a>
							</div>
						</td>
						<td class="cart_total">
							<p class="cart_total_price">$'.$value->tong.'</p>
						</td>
						<td class="cart_delete">
							<a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
						</td>
					</tr>';
		}
	}
	

?>
<section id="cart_items">
	<div class="container">
		<div class="breadcrumbs">
			<ol class="breadcrumb">
			  <li><a href="#">Home</a></li>
			  <li class="active">Shopping Cart</li>
			</ol>
		</div>
		<div class="table-responsive cart_info">
			<table class="table table-condensed">
				<thead>
					<tr class="cart_menu">
						<td class="image">Item</td>
						<td class="description"></td>
						<td class="price">Price</td>
						<td class="quantity">Quantity</td>
						<td class="total">Total</td>
						<td></td>
					</tr>
				</thead>
				<tbody>
					{!!$html!!}
				</tbody>
			</table>
		</div>
	</div>
</section> <!--/#cart_items-->
<section id="do_action">
	<div class="container">
		<div class="heading">
			<h3>What would you like to do next?</h3>
			<p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<div class="chose_area">
					<ul class="user_option">
						<li>
							<input type="checkbox">
							<label>Use Coupon Code</label>
						</li>
						<li>
							<input type="checkbox">
							<label>Use Gift Voucher</label>
						</li>
						<li>
							<input type="checkbox">
							<label>Estimate Shipping & Taxes</label>
						</li>
					</ul>
					<ul class="user_info">
						<li class="single_field">
							<label>Country:</label>
							<select>
								<option>United States</option>
								<option>Bangladesh</option>
								<option>UK</option>
								<option>India</option>
								<option>Pakistan</option>
								<option>Ucrane</option>
								<option>Canada</option>
								<option>Dubai</option>
							</select>
							
						</li>
						<li class="single_field">
							<label>Region / State:</label>
							<select>
								<option>Select</option>
								<option>Dhaka</option>
								<option>London</option>
								<option>Dillih</option>
								<option>Lahore</option>
								<option>Alaska</option>
								<option>Canada</option>
								<option>Dubai</option>
							</select>
						
						</li>
						<li class="single_field zip-field">
							<label>Zip Code:</label>
							<input type="text">
						</li>
					</ul>
					<a class="btn btn-default update" href="">Get Quotes</a>
					<a class="btn btn-default check_out" href="">Continue</a>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="total_area">
					<ul>
						<li>Cart Sub Total <span>$59</span></li>
						<li>Eco Tax <span>$2</span></li>
						<li>Shipping Cost <span>Free</span></li>
						<li>Total <span class="total">$61</span></li>
					</ul>
						<a class="btn btn-default update" href="">Update</a>
						<a class="btn btn-default check_out" href="">Check Out</a>
				</div>
			</div>
		</div>
	</div>
</section><!--/#do_action-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(e){
		$('.cart_quantity_up').click(function(){
			var id_product =$(this).closest('.tr_cart').attr('id');
			var sl=$(this).next().val();
			var price=$(this).closest('.tr_cart').find('.price').text().replace('$','');
			sl =parseFloat(sl)+1;
			$(this).closest('.tr_cart').find('.cart_quantity_input').val(sl);
			var tong=sl*price;	
			$(this).closest('.tr_cart').find('.cart_total_price').text(tong);	

			$.ajax({
				type:'post',
				url :'/up_ajax_cart',
				data:{
					"id_product":id_product,
					"button":"up",
					"_token": "{{ csrf_token() }}",
				},success:function(res){
					console.log(res);
				},error:function(res){
					console.log(res);
				},
			});
		});
		$('.cart_quantity_down').click(function(){
			var id_product =$(this).closest('.tr_cart').attr('id');	
			var sl=$(this).prev().val();
			if(sl>1){
				sl =parseFloat(sl)-1;
				var price=$(this).closest('.tr_cart').find('.price').text().replace('$','');
				$(this).closest('.tr_cart').find('.cart_quantity_input').val(sl);
				var tong=sl*price;	
				$(this).closest('.tr_cart').find('.cart_total_price').text(tong)
			}
			$.ajax({
				type:'post',
				url :'/down_ajax_cart',
				data:{
					"id_product":id_product,				
					"button":"down",
					"_token":"{{ csrf_token() }}",
				},success:function(res){
					console.log(res);
				},error:function(res){
					console.log(res);
				}
		 	});
		});
		$(".cart_quantity_delete").click(function(e){
				var id_product =$(this).closest('.tr_cart').attr('id');			
				$.ajax({
					type:'post',
					url :'/delete_ajax_cart',
					data:{
						"id_product" :id_product,
						"button" :"delete",
			 			_token: "{{ csrf_token() }}",						
						},success : function(res){
							console.log(res);
						},error:function(res){
							console.log(res);
						}
				});	
			});	
		
			var total=0;
			$(".cart_total_price").each(function(){

				 total+=parseFloat($(this).text().replace('$',''));
				
			});				
			$('span.total').text('$'+total);
					// $(this).closest('.tr_cart').find('.price').val(res);
					// var price=$(this).closest('.tr_cart').find('.price').text().replace('$','');
					// var sl=$(this).closest('.tr_cart').find('.cart_quantity_input').val();
					// var tong=sl*price;	
					// $(this).closest('.tr_cart').find('.cart_total_price').text(tong);		
	});
</script>
@endsection