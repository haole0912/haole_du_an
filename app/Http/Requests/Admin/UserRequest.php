<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'name'=>'required', 
        'email'=>'required|email|unique:users', 
        'phone'=>'required|integer', 
        'adress'=>'required',  
        'avatar'=>'image|mimes:jpeg,png,jpg,gif|max:1024'  
        ];
    }
    public function messages()
    {
        return[
         'required'=>':attribute :không được để trống',
         'email.email'=>':attribute :email sai định dạng',   
         'email.unique'=>':attribute :email đã tồn tại',   
         'integer'=>':attribute :ký tự nhập vào là số',
        
         'image'=>':attribute :file upload lên phải là hình ảnh', 
         'mimes'=>':attribute :Định dạng hình ảnh phải là :jpeg,png,jpg,gif', 
         'avatar.max'=>':attribute :hình ảnh upload phải dưới 1mb'    
        ];
    }
    public function attributes()
    {
        return[
        'name'=>'Full Name',
        'email'=>'Email',
        'phone'=>'Phone No',
        'adress'=>'Adress',
        'avatar'=>'Avatar'    
        ];
    }
}
