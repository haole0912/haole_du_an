<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Frontend\ProductRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\Frontend\ProductModel;
use App\Models\Frontend\CategoryModel;
use App\Models\Frontend\BrandModel;
use Image;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function my_product()
    {
        $table=ProductModel::all();
        return view ('frontend/product/my-product',['product'=>$table]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show_add()
    {
        $cate=CategoryModel::all();
        $brand=BrandModel::all();
        return view ('frontend/product/add-product',['category'=>$cate,'brand'=>$brand]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function add(ProductRequest $request)
    {
        $table=new ProductModel();
        $img=[];
        $data=$request->all();
        $file=$request->image; 
        if(!empty($file)) {    
            foreach($file as $image)
            {

                $name = $image->getClientOriginalName();
                $name_2 = "hinh85_".$image->getClientOriginalName();
                $name_3 = "hinh329_".$image->getClientOriginalName();
                
                $path = public_path('upload/product/' . $name);
                $path2 = public_path('upload/product/' . $name_2);
                $path3 = public_path('upload/product/' . $name_3);

                Image::make($image->getRealPath())->save($path);
                Image::make($image->getRealPath())->resize(85, 84)->save($path2);
                Image::make($image->getRealPath())->resize(329, 380)->save($path3);
                
                $img[] = $name;
            }
            $data['image']=json_encode($img); 
        }
        if($table->create($data)) {
             return redirect()->back()->with('success','Add Product thanh cong');
        }else{
            return redirect()->back()->withErrors('Add Product that bai');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_edit($id)
    {
        $table=ProductModel::FindOrFail($id);
        $cate=CategoryModel::all();
        $brand=BrandModel::all();
        return view ('frontend/product/edit-product',['product'=>$table,'category'=>$cate,'brand'=>$brand]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $check =$request->check;
        $data= $request->all();
        $table=ProductModel::FindOrFail($id);
        $img=json_decode($table->image);
        $file=$request->image;
        $pass=0;
        $countcheck=0;
        // $countdb=count($img);
        $countfile=0;
        if(!empty($check))
        {
            foreach($check as $key) {
            unset($img[$key]);
            $resetimg=array_values($img);  
            }
            $countcheck=count($resetimg);
            
        }
        if(!empty($file)) {
            $countfile=count($file);     
            if($countfile<=3&&$countcheck==0){
                $pass=1;
            }
            if($countfile<=2&&$countcheck==1){
                $pass=1;
            }
            if($countfile==1&&$countcheck==2){
                $pass=1;
            }   

                      
            // if($countdb==1&&$countfile<=2&&$countcheck==1){
            //     $pass=1;
            // }
            // if($countdb==1&&$countfile<=3&&$countcheck==0){
            //     $pass=1;
            // }
            // if($countdb==2&&$countfile==1&&$countcheck==2){
            //     $pass=1;
            // }
            // if($countdb==2&&$countfile<=2&&$countcheck==1){
            //     $pass=1;
            // }
            // if($countdb==2&&$countfile<=3&&$countcheck==0){
            //     $pass=1;
            // } 
            // if($countdb==3&&$countfile<=3&&$countcheck==0){           
            //     $pass=1;
            // }      
            // if($countdb==3&&$countfile==1&&$countcheck==2){            
            //     $pass=1;
            // }            
            // if($countdb==3&&$countfile<=2&&$countcheck==1){      
            //     $pass=1;
            // }
            if($pass==1)
            {
                foreach($file as $image)
                {

                    $name = $image->getClientOriginalName();
                    $name_2 = "hinh85_".$image->getClientOriginalName();
                    $name_3 = "hinh329_".$image->getClientOriginalName();
                    
                    $path = public_path('upload/product/' . $name);
                    $path2 = public_path('upload/product/' . $name_2);
                    $path3 = public_path('upload/product/' . $name_3);

                    Image::make($image->getRealPath())->save($path);
                    Image::make($image->getRealPath())->resize(85, 84)->save($path2);
                    Image::make($image->getRealPath())->resize(329, 380)->save($path3);
                    
                    $resetimg[] = $name;
                }
                $data['image']=json_encode($resetimg);
                // return redirect()->back()->with('success','so hinh khong vuot qua 3');
            }else {
                return redirect()->back()->withErrors('Tong so hinh anh da vuot qua 3');
            }
            
        }else {
            if(!empty($check)) {
                $data['image']=json_encode($resetimg);
                // return redirect()->back()->with('success','so hinh khong vuot qua 3');
            }else {
                $data['image']=$table->image;
                // return redirect()->back()->with('success','so hinh khong vuot qua 3');
            }
        }

        if($table->update($data)) {
             return redirect()->back()->with('success','Update Product thanh cong');
        }else{
            return redirect()->back()->withErrors('Update Product that bai');
        }


    
    }
    public function show_details($id) {
        $data=ProductModel::FindOrFail($id);
        return view ('frontend/product/product-details',['product'=>$data]);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=ProductModel::find($id);
        if($data->delete())
        {
            return redirect()->back()->with('success','delete Product thanh cong');
        }else{
            return redirect()->back()->withErrors('delete Product that bai');
        }
    }
}
