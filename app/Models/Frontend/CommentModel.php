<?php

namespace App\Models\Frontend;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommentModel extends Model
{
    protected $table='comment';
    protected $fillable=['id','id_blog','id_user','cmt','level','time','avatar','name'];
    public $timestamps=false;
}
