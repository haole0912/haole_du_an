<?php 
	$html="";
	if(!empty($cart)){
		foreach($cart as $value){
		$img=json_decode($value->image);
		$html.='	<tr class="tr_cart" id="'.$value->id.'">
						<td class="cart_product">
							<a href=""><img src="/upload/product/hinh85_'.$img[0].'" alt=""></a>
						</td>
						<td class="cart_description">
							<h4><a href="">'.$value->name.'</a></h4>
							<p>Web ID: '.$value->id.'</p>
						</td>
						<td class="cart_price">
							<p class="price">$'.$value->price.'</p>
						</td>
						<td class="cart_quantity">
							<div class="cart_quantity_button" >
								<a class="cart_quantity_up" href="#"> + </a>
								<input class="cart_quantity_input" type="text" name="quantity" value="'.$value->qty.'" autocomplete="off" size="2">
								<a class="cart_quantity_down" href="#"> - </a>
							</div>
						</td>
						<td class="cart_total">
							<p class="cart_total_price">$'.$value->tong.'</p>
						</td>
						<td class="cart_delete">
							<a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
						</td>
					</tr>';
		}
	}
	

?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
</head>
<body>
<section id="cart_items">
	<div class="container">
		<div class="table-responsive cart_info">
			<table class="table table-condensed">
				<thead>
					<tr class="cart_menu">
						<td class="image">Item</td>
						<td class="description"></td>
						<td class="price">Price</td>
						<td class="quantity">Quantity</td>
						<td class="total">Total</td>
						<td></td>
					</tr>
				</thead>
				<tbody>
					@if(!empty($cart))
					@foreach($cart as $value)
					<?php 
					$img=json_decode($value->image);

					?>
					<tr class="tr_cart" id="'.$value->id.'">
						<td class="cart_product">
							<a href=""><img src="/upload/product/hinh85_'.$img[0].'" alt=""></a>
						</td>
						<td class="cart_description">
							<h4><a href="">'.$value->name.'</a></h4>
							<p>Web ID: '.$value->id.'</p>
						</td>
						<td class="cart_price">
							<p class="price">$'.$value->price.'</p>
						</td>
						<td class="cart_quantity">
							<div class="cart_quantity_button" >
								<a class="cart_quantity_up" href="#"> + </a>
								<input class="cart_quantity_input" type="text" name="quantity" value="'.$value->qty.'" autocomplete="off" size="2">
								<a class="cart_quantity_down" href="#"> - </a>
							</div>
						</td>
						<td class="cart_total">
							<p class="cart_total_price">$'.$value->tong.'</p>
						</td>
						<td class="cart_delete" id="'.$value->id.'">
							<a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
						</td>
					</tr>
					@endforeach
					@endif

					<tr>
							<td colspan="4">&nbsp;</td>
							<td colspan="2">
								<table class="table table-condensed total-result">
									<tr>
										<td>Cart Sub Total</td>
										<td>$59</td>
									</tr>
									<tr>
										<td>Exo Tax</td>
										<td>$2</td>
									</tr>
									<tr class="shipping-cost">
										<td>Shipping Cost</td>
										<td>Free</td>										
									</tr>
									<tr>
										<td>Total</td>
										<td><span class="total">$61</span></td>
									</tr>
								</table>
							</td>
						</tr>
				</tbody>
			</table>
		</div>
	</div>
</section> <!--/#cart_items-->
</body>
</html>