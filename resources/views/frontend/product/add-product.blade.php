@extends('frontend.layouts.app')
@section('content')
<div class="blog-post-area">
	<h2 class="title text-center">Add Product</h2>
	<div class="signup-form">
		<form method="POST" enctype="multipart/form-data">
			@csrf
			<input type="text" placeholder="Name" name="name">
			<input type="number"  placeholder="Price" name="price">
			
			<select name="id_category">
				@foreach($category as $val)
				<option value="{{$val->id}}">{{$val->name_country}}</option>
				@endforeach
			</select>
			<select name="id_brand">
				@foreach($brand as $value)
				<option value="{{$value->id}}">{{$value->name_country}}</option>
				@endforeach
			</select>
			<select  id="id_sale" onchange="hide()">
				<option value="0">Sale</option>
				<option value="1">New</option>
			</select>
			<div class="col-sm-3">				
				<div id="%_sale" style="display:flex; align-items: center;">
					<input type="text" name="sale">
					<label>%</label>
				</div>
			</div>
			<input type="text"  placeholder="Company Profile" name="company">
			<label>Image</label>					
			<input type="file" name="image[]" multiple>
			<textarea name="details"></textarea>
			<button type="submit" class="btn btn-default">Add Product</button>
			@if($errors->any())								
					<div class="alert alert-danger alert-dismissible">
						<ul>
							@foreach($errors->all() as $error )
							<li>{{$error}}</li>
							@endforeach
						</ul>
					</div>
			@endif
			@if(session('success'))
				<h1 class="alert alert-success">{{session('success')}}</h1>
			@endif
		</form>
	</div>
</div>	
<script type="text/javascript">
	function hide(){
		let id=document.getElementById('id_sale').value;
		let div1 = document.getElementById("%_sale");
		if(id=="0") {
			div1.style.visibility = "visible";
		}else {
			div1.style.visibility = "hidden";
		}
	}

</script>
@endsection