<?php

namespace App\Models\Frontend;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RateModel extends Model
{
    protected $table='rate';
    protected $fillable=['rate','id_user','id_blog','time'];
}
