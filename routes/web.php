<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// FRONTEND

Route::get('member/register','Frontend\MemberController@show_reg');
Route::post('member/register','Frontend\MemberController@register');

Route::get('member/login','Frontend\MemberController@show_log');
Route::post('member/login','Frontend\MemberController@login');

Route::get('member/logout','Frontend\MemberController@logout');

Route::get('member/blog','Frontend\BlogController@index');
Route::get('member/blog-details/{id}','Frontend\BlogController@details');

Route::post('ajax_rate/{id}','Frontend\BlogController@ajax_rate');
Route::post('/postcmt/{id}','Frontend\BlogController@comment');

Route::get('member/account/update','Frontend\MemberController@show_account');
Route::post('member/account/update','Frontend\MemberController@update');

Route::get('member/account/my-product','Frontend\ProductController@my_product');
Route::get('member/account/add-product','Frontend\ProductController@show_add');
Route::post('member/account/add-product','Frontend\ProductController@add');
Route::get('member/account/edit-product/{id}','Frontend\ProductController@show_edit');
Route::post('member/account/edit-product/{id}','Frontend\ProductController@edit');
Route::get('member/account/delete-product/{id}','Frontend\ProductController@destroy');

Route::get ('member/home','Frontend\HomeController@index');

Route::get('/member/product-details/{id}','Frontend\ProductController@show_details');

Route::post('ajax_cart','Frontend\CartController@ajax_add_to_cart');
Route::get('/member/cart','Frontend\CartController@show_cart');
Route::post('/up_ajax_cart','Frontend\CartController@ajax_cart');
Route::post('/down_ajax_cart','Frontend\CartController@ajax_cart');
Route::post('/delete_ajax_cart','Frontend\CartController@ajax_cart');

Route::get('/member/checkout','Frontend\MailController@show_checkout');
Route::post('/member/checkout','Frontend\MailController@register');

Route::get('/member/search','Frontend\SearchController@view_search');
Route::post('/ajax_search','Frontend\SearchController@search');

Route::get('/member/home/search_advance','Frontend\SearchController@view_search_advance');
Route::post('/ajax_search_advance','Frontend\SearchController@search_advance');

Route::get('/member/home/search_price','Frontend\SearchController@view_search_price');
Route::post('/ajax_search_price','Frontend\SearchController@search_price');

// ADMIN 
Auth::routes();
Route::get('/home','Admin\DashboardController@index');
Route::get('/user','Admin\UserController@hienthi_users');
Route::post('/user','Admin\UserController@update_users');
Route::get('/country','Admin\CountryController@index');
Route::get('/add-country','Admin\CountryController@show_add_country');
Route::post('/add-country','Admin\CountryController@add_country');
Route::get('/edit-country/{id}','Admin\CountryController@show_edit');
Route::post('/edit-country/{id}','Admin\CountryController@edit');
Route::get('/country/{id}','Admin\CountryController@destroy');
Route::get('/blog','Admin\BlogController@index');
Route::get('/add-blog','Admin\BlogController@show_create');
Route::post('/add-blog','Admin\BlogController@create');
Route::get('/edit-blog/{id}','Admin\BlogController@show_edit');
Route::post('/edit-blog/{id}','Admin\BlogController@edit');
Route::get('/blog/{id}','Admin\BlogController@destroy');


Route::get('/test','Frontend\MailController@index');