@extends('admin.layouts.app')
@section('content')
	<div class="page-breadcrumb">
	    <div class="row">
	        <div class="col-5 align-self-center">
	            <h4 class="page-title">Country</h4>
	        </div>
	        <div class="col-7 align-self-center">
	            <div class="d-flex align-items-center justify-content-end">
	                <nav aria-label="breadcrumb">
	                    <ol class="breadcrumb">
	                        <li class="breadcrumb-item">
	                            <a href="#">Home</a>
	                        </li>
	                        <li class="breadcrumb-item active" aria-current="page">Country</li>
	                    </ol>
	                </nav>
	            </div>
	        </div>
	    </div>
	</div> 
	<div class="table-responsive">
	<table class="table">
	    <thead>
	        <tr>
	            <th scope="col">Id</th>
	            <th scope="col">Name Country</th>
	            <th scope="col">Image</th>
	            <th scope="col">Description</th>
	            <th scope="col">Action</th>
	        </tr>
	    </thead>
	    <tbody>
	    	@foreach($data as $value)
	        <tr>
	            <th scope="row">{{$value->id}}</th>
	            <td>{{$value->name_country}}</td>
	            <td>{{$value->image}}</td>
	            <td>{{$value->description}}</td>	     
	            <td><a href="/edit-country/{{$value->id}}">Edit</a></td>
	            <td><a href="/country/{{$value->id}}">Delete</a></td>
	        </tr>
	        @endforeach

	    </tbody>

	</table>
	{!! $data->links('pagination::bootstrap-4') !!}
	</div>

	<div class="form-group">
	    <div class="col-sm-12">
	        <a href="/add-country" class="btn btn-success">Add Country</a>
	    </div>
	</div>
	@if($errors->all())
		@foreach($errors as $error)
		<p class="alert alert-danger">{{$error}}</p>
		@endforeach
	@endif
	@if(session('success'))
	<p class="alert alert-success">{{session('success')}}</p>
	@endif               
@endsection