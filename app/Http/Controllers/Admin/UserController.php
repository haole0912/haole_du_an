<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\UserRequest;
use App\Models\Admin\UserModel;
use App\Models\Admin\CountryModel;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
        $this->middleware('auth');
    }
    public function hienthi_users()
    {   $id=Auth::id();
        $country=CountryModel::all();
        $data=UserModel::FindOrFail($id);
        return view ('admin/user/user',['data'=>$data,'country'=>$country]);
    }
    public function update_users(Request $request )
    {
        // echo 'da xong';
        $id=Auth::id();
        $data=UserModel::FindOrFail($id);
        $input=$request->all();
        $file=$request->avatar;
        if(!empty($file))
        {
            $input['avatar']=$file->getClientOriginalName();
        }
        if(!empty($input['password']))
        {
            $input['password']=bcrypt($input['password']);           
        }
        else
        {
           $input['password']=$data->password; 
           
        }
        if($data->update($input))
        {
            if(!empty($file)){
                $file->move('upload/user/avatar', $file->getClientOriginalName());
            }
            return redirect()->back()->with('success',('Update profile Thanh Cong.'));
        } 
        else 
        {
            return redirect()->back()->withErrors('Update profile That bai.');
        }
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
