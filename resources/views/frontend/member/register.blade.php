@extends('frontend.layouts.app')
@section('content')

		<div class="col-sm-4">
			<h2>New User Signup!</h2>
					<div class="signup-form"><!--sign up form-->
						
						<form method="POST">
							@csrf
							<input type="text" placeholder="Name" name="name" />
							<input type="email" placeholder="Email Address" name="email" />
							<input type="password" placeholder="Password" name="password" />
							<input type="text"  placeholder="Phone No" name="phone">
							<input type="text"  placeholder="Address" name="address">
							<select name="id_country">
								@foreach($country as $value)
								<option value="{{$value->id}}">{{$value->name_country}}</option>
								@endforeach
							</select>
							<label>Avatar</label>
							<input type="file" name="avatar">
							<button type="submit" class="btn btn-default">Signup</button>
							@if($errors->any())								
									<div class="alert alert-danger alert-dismissible">
										<ul>
											@foreach($errors->all() as $error )
											<li>{{$error}}</li>
											@endforeach
										</ul>
									</div>
							@endif
							@if(session('success'))
								<h1 class="alert alert-success">{{session('success')}}</h1>
							@endif
						</form>
					</div><!--/sign up form-->
		</div>	

@endsection