@extends('frontend.layouts.app')
@section('content')
<div class="col-sm-9 padding-right">
	<h2 class="title text-center">Features Items</h2>
<div class="features_items"><!--features_items-->
@foreach($product as $value)
<?php 
	$img =json_decode($value->image);
?>
	<div class="col-sm-4">
		<div class="product-image-wrapper">
			<div class="single-products">
					<div class="productinfo text-center">
						<img src="/upload/product/{{$img[0]}}" alt="" />
						<h2>${{$value->price}}</h2>
						<p>{{$value->name}}</p>
						<a  class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
					</div>
					<div class="product-overlay">
						<div class="overlay-content">
							<h2>${{$value->price}}</h2>
							<a href="/member/product-details/{{$value->id}}">{{$value->name}}</a>
							<input type="hidden" name="" value="{{$value->id}}">
							<a  class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
						</div>
					</div>
			</div>
			<div class="choose">
				<ul class="nav nav-pills nav-justified">
					<li><a href=""><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
					<li><a href=""><i class="fa fa-plus-square"></i>Add to compare</a></li>
				</ul>
			</div>
		</div>
	</div>
@endforeach	
{!! $product->links('pagination::bootstrap-4') !!}
</div><!--features_items-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$(".add-to-cart").click(function(){
		var id=$(this).prev().val();
		$.ajax({
			type:'POST',
			url:'/ajax_cart',
			data:{
				id:id,
				_token:"{{ csrf_token() }}",
			},success:function(res){
				console.log(res);
			},error:function(res){
				// console.log(res);
			}
		});
	});	
});	


</script>
@endsection