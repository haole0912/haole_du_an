<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\BlogModel;
use App\Models\Frontend\RateModel;
use App\Models\Frontend\CommentModel;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $blog =BlogModel::paginate(3);
        return view ('/frontend/blog/blog',['blog'=>$blog]);
    }
    public function details($id)
    {   
        $cmt =CommentModel::where('id_blog',$id)->get();   
        $rate= RateModel::where('id_blog',$id)->get();
        $details_blog=BlogModel::FindOrFail($id);
        $previous =BlogModel::where ('id','<',$id)->max('id');
        $next =BlogModel::where ('id','>',$id)->min('id');
        return view ('/frontend/blog/blog-details',['details_blog'=>$details_blog,'previous'=>$previous,'next'=>$next ,'rate'=>$rate,'comment'=>$cmt]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ajax_rate(Request $request,$id)
    {
        $time = Carbon::now();
        $data['rate']=$request->rate;
        $data['id_user']=Auth::id();
        $data['id_blog']=$id;
        $data['time']=$time->toDateTimeString();
        $data['avatar']=Auth::user()->avatar;
        if(RateModel::create($data)){
            return 'them thanh cong';
        }else{
            return 'them that bai';
        }
        
    }
    // public function show_comment($id)
    // {
    //     $cmt=CommentModel::where('id_blog',$id)->get();
    //     return view ('/frontend/blog/blog-details',['cmt']=>$cmt);
    // }
    public function comment (Request $request,$id)
    {   
        $time=new Carbon();
        $data['cmt']=$request->cmt;
        $data['id_blog']=$id;
        $data['id_user']=Auth::id();
        $data['name']=Auth::user()->name;
        $data['level']=0;
        if($request->id_cmt) {
            $data['level']=$request->id_cmt;
        }
        $data['avatar']=Auth::user()->avatar;
        $data['time']=$time->toDateTimeString();
        CommentModel::create($data);
        return redirect()->back();
    }
        
        
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
