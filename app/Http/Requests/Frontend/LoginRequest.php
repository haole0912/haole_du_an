<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'=>'required|email'
            'password'=>'required|'
        ];
    }
    public function messages()
    {
        return [
           'email.required'=>'Vui Long nhap Email',
           'password.required'=>'Vui long Nhap Password',
           'email'=>':attribute sai dinh dang mail' 

        ];
    }
    public function attributes()
    {
        return[
           'email'=>'Email',
           'password'=>'Mat Khau' 

        ];
    }
}
