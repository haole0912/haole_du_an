<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Frontend\ProductModel;
use App\Models\Frontend\CategoryModel;
use App\Models\Frontend\BrandModel;
class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function view_search() {
        return view ('frontend/search/search');
    }
    public function search(Request $request) {
        $name=$request->name;
        $product=ProductModel::where('name','like',"%{$name}%")->get()->toArray();

        return $product;

    }
    public function view_search_advance(){
        $cate=CategoryModel::all();
        $brand=BrandModel::all();
        return view ('frontend/search/search-advance',['category'=>$cate,'brand'=>$brand]);
    }
    public function search_advance(Request $request){
        $name=$request->name;
        if(!empty($request->price))
        {
          $price=explode('-',$request->price);
            
        }
        //whereBetween('price',array($price[0],$price[1]))
        //orWhere('price','BETWEEN'.$price[0].'AND'.$price[1])
        $product=ProductModel::where('name','like',"%$name%")->whereBetween('price',[$price[0],$price[1]])->get()->toArray();
        return $product;
    }


    public function view_search_price(){

        return view ('frontend/search/search-price');
    }
    public function search_price(Request $request){
        if(!empty($request->price))
        {
          $price=explode(':',$request->price);
        }
        //whereBetween('price',array($price[0],$price[1]))
        //orWhere('price','BETWEEN'.$price[0].'AND'.$price[1])
        $product=ProductModel::whereBetween('price',[$price[0],$price[1]])->get()->toArray();
        return $product;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
