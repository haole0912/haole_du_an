@extends('frontend.layouts.app')
@section('content')
<div class="col-sm-9 padding-right">
	<h2 class="title text-center">Features Items</h2>
		<button class="search_advance">Search</button>
		<div class="features_items"><!--features_items-->
	
		</div><!--features_items-->
	</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){	
	$(".search_advance").click(function(){
		var html=''
		var price=$('.tooltip-inner').text();			
		$.ajax({
			type:'post',
			url:'/ajax_search_price',
			data:{
				'price':price,
				'_token':"{{ csrf_token() }}",
			},success:function(res){
					Object.keys(res).map((key,index)=>{
					var img =JSON.parse(res[key]['image']);
					html +="<div class='col-sm-4'>"+
								"<div class='product-image-wrapper'>"+
									"<div class='single-products'>"+
										"<div class='productinfo text-center'>"+
											"<img src='/upload/product/"+img[0]+"'/>"+
											"<h2>"+res[key]['price']+"</h2>"+
											"<p>"+res[key]['name']+"</p>"+
											"<a  class='btn btn-default add-to-cart'><i class='fa fa-shopping-cart'></i>Add to cart</a>"+
										"</div>"+
										"<div class='product-overlay'>"+
											"<div class='overlay-content'>"+
												"<h2>"+res[key]['price']+"</h2>"+
												"<a href='/member/product-details/"+res[key]['id']+"'>"+res[key]['name']+"</a>"+
												"<input type='hidden' value='"+res[key]['id']+"'>"+
												"<a  class='btn btn-default add-to-cart'><i class='fa fa-shopping-cart'></i>Add to cart</a>"+
											"</div>"+
										"</div>"+
									"</div>"+
									"<div class='choose'>"+
										"<ul class='nav nav-pills nav-justified'>"+
											"<li><a ><i class='fa fa-plus-square'></i>Add to wishlist</a></li>"+
											"<li><a ><i class='fa fa-plus-square'></i>Add to compare</a></li>"+
										"</ul>"+
									"</div>"+
								"</div>"+
							"</div>";
				});
				$('.features_items').html(html);
			}
		});
	});
	
});	


</script>
@endsection