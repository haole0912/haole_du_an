@extends('frontend.layouts.app')
@section('content')
<?php 
	$html="";
	$points=0;
	$count=0;
	$cha="";
	$con="";
	foreach($rate as $value){
		$points +=$value->rate;
		$count+=1;
	}
	if($rate->isEmpty()){
		$count=1;
	}
	
	switch(round($points/$count)){
		case 1:
		$html='<div class="star_1 ratings_stars ratings_over"><input value="1" type="hidden"></div>
			   <div class="star_2 ratings_stars "><input value="2" type="hidden"></div>
			   <div class="star_3 ratings_stars "><input value="3" type="hidden"></div>
			   <div class="star_4 ratings_stars "><input value="4" type="hidden"></div>
			   <div class="star_5 ratings_stars "><input value="5" type="hidden"></div>';
		break;
		case 2:
		$html='<div class="star_1 ratings_stars ratings_over"><input value="1" type="hidden"></div>
			   <div class="star_2 ratings_stars ratings_over"><input value="2" type="hidden"></div>
			   <div class="star_3 ratings_stars "><input value="3" type="hidden"></div>
			   <div class="star_4 ratings_stars "><input value="4" type="hidden"></div>
			   <div class="star_5 ratings_stars "><input value="5" type="hidden"></div>';
		break;
		case 3:
		$html='<div class="star_1 ratings_stars ratings_over"><input value="1" type="hidden"></div>
			   <div class="star_2 ratings_stars ratings_over"><input value="2" type="hidden"></div>
			   <div class="star_3 ratings_stars ratings_over"><input value="3" type="hidden"></div>
			   <div class="star_4 ratings_stars "><input value="4" type="hidden"></div>
		   	   <div class="star_5 ratings_stars "><input value="5" type="hidden"></div>';
		break;
		case 4:
		$html='<div class="star_1 ratings_stars ratings_over"><input value="1" type="hidden"></div>
			   <div class="star_2 ratings_stars ratings_over"><input value="2" type="hidden"></div>
			   <div class="star_3 ratings_stars ratings_over"><input value="3" type="hidden"></div>
			   <div class="star_4 ratings_stars ratings_over"><input value="4" type="hidden"></div>
			   <div class="star_5 ratings_stars "><input value="5" type="hidden"></div>';
		break;
		case 5:
		$html='<div class="star_1 ratings_stars ratings_over"><input value="1" type="hidden"></div>
			   <div class="star_2 ratings_stars ratings_over"><input value="2" type="hidden"></div>
			   <div class="star_3 ratings_stars ratings_over"><input value="3" type="hidden"></div>
			   <div class="star_4 ratings_stars ratings_over"><input value="4" type="hidden"></div>
			   <div class="star_5 ratings_stars ratings_over"><input value="5" type="hidden"></div>';
		break;
		default:
		$html='<div class="star_1 ratings_stars "><input value="1" type="hidden"></div>
			   <div class="star_2 ratings_stars "><input value="2" type="hidden"></div>
			   <div class="star_3 ratings_stars "><input value="3" type="hidden"></div>
			   <div class="star_4 ratings_stars "><input value="4" type="hidden"></div>
			   <div class="star_5 ratings_stars "><input value="5" type="hidden"></div>';
		break;
	};
		

	

?>
<div class="blog-post-area">
	<h2 class="title text-center">Latest From our Blog</h2>
	<div class="single-blog-post">
		<h3>{{$details_blog->title}}</h3>
		<div class="post-meta">
			<ul>
				<li><i class="fa fa-user"></i> Mac Doe</li>
				<li><i class="fa fa-clock-o"></i> 1:33 pm</li>
				<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
			</ul>
			<!-- <span>
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
				<i class="fa fa-star-half-o"></i>
			</span> -->
		</div>
		<a >
			<img src="/upload/blog/{{$details_blog->image}}" alt="">
		</a>
		<p>{{$details_blog->description}}</p> <br>
		<div class="pager-area">
			<ul class="pager pull-right">
				@if($previous)
				<li><a href="{{URL::to('/member/blog-details/'.$previous)}}">Pre</a></li>
				@endif
				@if($next)
				<li><a href="{{URL::to('/member/blog-details/'.$next)}}">Next</a></li>
				@endif
			</ul>
		</div>
	</div>
</div><!--/blog-post-area-->
	<div class="rating-area">
		<div class="rate">
	        <div class="vote">
			{!!$html!!}
	            <span class="rate-np">{{count($rate)}} Vote</span>
	        </div> 
	    </div>
	</div><!--/rating-area-->
	<div class="response-area">
		<h2>3 RESPONSES</h2>
		<ul class="media-list">
	@foreach( $comment as $cmtcha) 
		<?php 
			$temp = explode(' ',$cmtcha->time);
		?>	
			@if($cmtcha->level==0) 
			<li class="media">
				<a class="pull-left" href="#">
					<img class="media-object" style=" width :120px ;height :86px " src="/upload/user/avatar/{{$cmtcha->avatar}}" alt="">
				</a>
				<div class="media-body">
					<ul class="sinlge-post-meta">
						<li><input value="{{$cmtcha->id}}" type="" class="id"></li>
						<li><i class="fa fa-user"></i>{{$cmtcha->name}}</li>
						<li><i class="fa fa-clock-o"></i>{{$temp[1]}}</li>
						<li><i class="fa fa-calendar"></i>{{$temp[0]}}</li>
					</ul>
					<p>{{$cmtcha->cmt}}</p>
					<a class="btn btn-primary reply"><i class="fa fa-reply"></i>Replay</a>
				</div>
			</li>
			@endif
			@foreach($comment as $cmtcon) 
				@if($cmtcon->level==$cmtcha->id)
					<li class="media second-media">
						<a class="pull-left" href="#">
							<img class="media-object" style=" width :120px ;height :86px " src="/upload/user/avatar/{{$cmtcon->avatar}}" alt="">
						</a>
						<div class="media-body">
							<ul class="sinlge-post-meta">
								<li><input value="{{$cmtcon->id}}" type="" class="id"></li>
								<li><i class="fa fa-user"></i>{{$cmtcon->name}}</li>
								<li><i class="fa fa-clock-o"></i> {{$temp[1]}}</li>
								<li><i class="fa fa-calendar"></i>{{$temp[0]}}</li>
							</ul>
							<p>{{$cmtcon->cmt}}</p>
							<a class="btn btn-primary reply"><i class="fa fa-reply"></i>Replay</a>
						</div>
					</li>
				@endif
			@endforeach	
	@endforeach
					 
					
				
	
		</ul>					
	</div><!--/Response-area-->

	<div class="replay-box">
		<div class="row">
			<form method="POST" action="/postcmt/{{$details_blog->id}}">
				@csrf
				<div class="col-sm-12">
					<h2>Leave a replay</h2>
					
					<div class="text-area">

						<div class="blank-arrow">
							@if(Auth::check())
							<label>{{Auth::user()->name}}</label>
							@endif
						</div>
						<span>*</span>
						<textarea name="cmt" rows="11" class="cmt"></textarea>
						<button type="submit"><input name="id_cmt" type="" class="id_cmt">Post Comment</button>
						<p class="alert alert-success" ></p>
					</div>
				</div>
			</form>
		</div>
	</div><!--/Repaly Box-->
<script src="{{asset('frontend/js/jquery-1.9.1.min.js')}}"></script>  
<script>
	if(screen.width <= 736){
        document.getElementById("viewport").setAttribute("content", "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no");
    }
	$(document).ready(function(){
		var check="{{Auth::check()}}";

		$('a.reply').click(function(){
			var id =$(this).prev().prev().find('input.id').val();
			$(".id_cmt").val(id);
		});

		$('.ratings_stars').hover(
            // Handles the mouseover
            function() {
                $(this).prevAll().andSelf().addClass('ratings_hover');
                // $(this).nextAll().removeClass('ratings_vote'); 
            },
            function() {
                $(this).prevAll().andSelf().removeClass('ratings_hover');
                // set_votes($(this).parent());
            }
        );
		$('.ratings_stars').click(function() {
			var Values =  $(this).find("input").val();
	        if(check) {
	        	if ($(this).hasClass('ratings_over')) {
	            $('.ratings_stars').removeClass('ratings_over');
	            $(this).prevAll().andSelf().addClass('ratings_over');
		        } else {
		        	$(this).prevAll().andSelf().addClass('ratings_over');
		        }	 
	 	    
			    $.ajax({
			    	type :'post',
			    	url:"/ajax_rate/{{$details_blog->id}}",
			    	data:{
			    		rate:Values,
			    		_token:"{{csrf_token()}}"
			    	},success:function(res)
			    	{
			    		console.log(res);
			    	}
			    });
	        }
	        else {
	        	alert('VUI LONG DANG NHAP DE RATING ');
	        }        	
		}); 
		$("form").submit(function() {
			var cmt =$(".cmt").val();
			if(check) {
				if(cmt=="") {
					$(".alert").text("VUI LONG NHAP COMMENT");
					return false;
				}else{
					return true;
				}
			}else {
				alert('VUI LONG DANG NHAP DE COMMENT');
			}
			return false;
		}); 
		
	});
</script>
@endsection