@extends('frontend.layouts.app')
@section('content')
<?php 


?>
<div class="col-sm-9">
	<div class="table-responsive cart_info">
		<table class="table table-condensed">
			<thead>
				<tr class="cart_menu">
					<td class="id">id</td>
					<td class="image">image</td>
					<td class="description">name</td>
					<td class="price">price</td>
					<td class="total">action</td>
					
				</tr>
			</thead>
			<tbody>
				@foreach($product as $value)
				<tr>
					<td class="cart_id">
						<h4><a href="">{{$value->id}}</a></h4>	
					</td>

					<td class="cart_product">
					<?php 
					$img =json_decode($value->image);
					?>
						<a><img style="width: 50px ;height: 50px"  src="/upload/product/{{$img[0]}}" alt=""></a>
						
					</td>

					<td class="cart_description">
						<h4><a href="">{{$value->name}}</a></h4>	
					</td>

					<td class="cart_price">
						<p>${{$value->price}}</p>
					</td>
					
					<td class="cart_total">
						<a href="/member/account/edit-product/{{$value->id}}">edit</a>
						<a href="/member/account/delete-product/{{$value->id}}">delete</a>
					</td>								
				</tr>
				@endforeach						
			</tbody>
		</table>
	</div>
<a class="btn btn-primary" href="/member/account/add-product">Add Product</a>
@if($errors->all())
	@foreach($errors as $error)
	<p class="alert alert-danger">{{$error}}</p>
	@endforeach
@endif
@if(session('success'))
<p class="alert alert-success">{{session('success')}}</p>
@endif 	
</div>
@endsection