<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'name'=>'required',  
        'price'=>'required|integer', 
        'image[]'=>'image|mimes:jpeg,png,jpg,gif|max:1024'  
        ];
    }
    public function attributes() {
        return [
        'name'=>'Ten San Pham',
        'price'=>'Gia',
        'image'=>'Hinh Anh'    
        ];
    }
    public function messages()
    {
        return[
         'required'=>':attribute :không được để trống',
         'integer'=>':attribute :ký tự nhập vào là số',
         'image'=>':file upload lên phải là hình ảnh', 
         'mimes'=>':attribute :Định dạng phải là :jpeg,png,jpg,gif', 
         'image.max'=>':attribute : upload phải dưới 1mb'    
        ];
    }
}
